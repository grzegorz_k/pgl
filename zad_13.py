import json
from datetime import datetime


def compare_age(person1, person2):
    age1 = datetime.strptime(person1["date_of_birth"], '%d-%m-%Y')
    age2 = datetime.strptime(person2["date_of_birth"], '%d-%m-%Y')
    if age1 < age2:
        return person1
    else:
        return person2


def display_name_and_surname_of_the_oldest_employee():
    path = input("podaj plik: ")
    try:
        file = open(path, "r")
        json_data = file.read()
        file.close()
        data = json.loads(json_data)
        oldest_employee = data["employees"][0]
        for i in range(1, len(data["employees"])):
            oldest_employee = compare_age(data["employees"][i], oldest_employee)
        print(f"Name: {oldest_employee['name']}\nSurname: {oldest_employee['surname']}")
    except FileNotFoundError:
        print("nie znaleziono pliku")


display_name_and_surname_of_the_oldest_employee()
