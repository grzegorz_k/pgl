def check_number(number):
    for i in range(2, (number // 2) + 1):
        if number % i == 0:
            return False
    return True


def print_prime_numbers():
    try:
        number = int(input("podaj liczbe dwucyfrowa calkowita"))
        if 9 < number < 100:
            for num in range(2, number + 1):
                if check_number(num):
                    print(num)
        else:
            print("zły przedział")
    except ValueError:
        print("zły typ")


print_prime_numbers()
